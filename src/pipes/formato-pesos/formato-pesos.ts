import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatoPesos',
})
export class FormatoPesosPipe implements PipeTransform {
  transform(value: number) {
    var formatter = new Intl.NumberFormat('CLP');
    return '$ ' + formatter.format(value < 0 ? 0 : value).replace(/,/g, '.');
  }
}