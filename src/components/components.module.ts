import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CredencialComponent } from './credencial/credencial';
import { PreloadImage } from './preload-image/preload-image';
import { ShowHideContainer } from './show-hide-password/show-hide-container';
import { ShowHideInput } from './show-hide-password/show-hide-input';;
import { QRCodeModule } from 'angular2-qrcode';

@NgModule({
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
	declarations: [
    PreloadImage,
    ShowHideContainer,
    ShowHideInput,
    CredencialComponent

  ],
	imports: [
    QRCodeModule
  ],
	exports: [
    ShowHideContainer,
    ShowHideInput,
    PreloadImage,
    CredencialComponent

  ]
})
export class ComponentsModule {}