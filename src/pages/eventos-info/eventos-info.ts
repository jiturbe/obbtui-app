import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';

@Component({
  selector: 'eventos-info-page',
  templateUrl: 'eventos-info.html',
})
export class EventosInfoPage {
  evento: any;
  id: string;
  loading: any;

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public dialogs: Dialogs
  ) {
    let env = this;
    env.evento = navParams.get('evento');
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.id = localStorage.getItem('id');
    console.log(env.evento);
  }
}