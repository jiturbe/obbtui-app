import { Component } from '@angular/core';
import { App, Events, LoadingController, NavController, Platform } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Dialogs } from '@ionic-native/dialogs';
import { AccesoPage } from '../acceso/acceso';
import { PortalAcademicoProvider } from '../../providers/portal-academico/portal-academico';
import { AulaVirtualProvider } from '../../providers/aula-virtual/aula-virtual';
import { PeriodoProvider } from '../../providers/periodo/periodo';
import { GlobalVar } from '../../config';

@Component({
  selector: 'perfil-page',
  templateUrl: 'perfil.html'
})
export class PerfilPage {
  loading: any;
  perfil: any;
  str_sinConexion: string;

  constructor(
    public app: App,
    public nav: NavController,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public events: Events,
    public aulaVirtualProvider: AulaVirtualProvider,
    public portalAcademicoProvider: PortalAcademicoProvider,
    public periodoProvider: PeriodoProvider,
    private ga: GoogleAnalytics,
    private dialogs: Dialogs
  ) {
    let env = this;
    let token = localStorage.getItem('token');
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';
    env.perfil = {
      fotografia: localStorage.getItem('fotografia'),
      movilidad: localStorage.getItem('movilidad'),
      nombres: localStorage.getItem('nombres'),
      apellidos: localStorage.getItem('apellidos'),
      codCarrera: localStorage.getItem('codCarrera'),
      carrera: localStorage.getItem('carrera'),
      descEstado: localStorage.getItem('descEstado'),
      periodo: {
        periodo: null,
        anio: null
      }
    };

    // Verificar sesión del dispositivo
    // env.events.publish('verificarSesion');

    env.periodo(token);
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-perfil');
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }

  editarPerfil() {
    let env = this;
    let token = localStorage.getItem('token');
    env.showCargando();
    env.portalAcademicoProvider.getUrlPortal(token)
      .subscribe(function(dataPortal) {
        env.hideCargando();
        console.log(dataPortal);
        if(dataPortal.status == 'ERROR') {
          env.alerta(dataPortal.error.message);
        } else if(dataPortal.status == 'OK') {
          window.open(dataPortal.data.url, '_system');
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  cambiarClave() {
    window.open(GlobalVar.OLVIDAR_CONTRASENA, '_system');
  }

  aulaVirtual() {
    let env = this;
    let token = localStorage.getItem('token');
    env.showCargando();
    env.aulaVirtualProvider.getUrlAula(token)
      .subscribe(function(dataAula) {
        env.hideCargando();
        console.log(dataAula);
        if(dataAula.status == 'ERROR') {
          env.alerta(dataAula.error.message);
        } else if(dataAula.status == 'OK') {
          window.open(dataAula.data.url, '_system');
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  cerrar() {
    let env = this;
    localStorage.clear();
    env.app.getRootNav().setRoot(AccesoPage);
    env.nav.popToRoot();
  }

  salir() {
    let env = this;
    let titulo = 'Alerta';
    let botones = ['Salir', 'Quedarse'];

    if(env.platform.is('cordova')) {
      env.dialogs.confirm('¿Estás seguro(a) que deseas salir?', titulo, botones)
        .then(function(salir) {
          console.log('Diálogo cerrado');
          if(salir != 0) {
            env.cerrar();
          }
        })
        .catch(function(e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error('No se puede mostrar un mensaje de confirmación en navegador.');
      env.cerrar();
    }
  }

  periodo(token: string) {
    let env = this;
    env.showCargando();
    env.periodoProvider.getPeriodo(token)
      .subscribe(function(dataPeriodo) {
        env.hideCargando();
        if(dataPeriodo.status == 'ERROR') {
          env.alerta(dataPeriodo.error.message);
        } else if(dataPeriodo.status == 'OK') {
          env.perfil.periodo.anio = dataPeriodo.data.anio;
          env.perfil.periodo.semestre = dataPeriodo.data.semestre;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }
}